# 简介

本项目是2015年春季修[黄亮](http://icpcs.lzu.edu.cn/lhuang/index.html)老师的非线性物理课上完成的作业以及重复教材上一些结果的整理汇总，因为课程本身并不容易，希望给后来的同学一些帮助。

教材选用Edward Ott所著的[Chaos in Dynamical Systems](http://www.amazon.com/Chaos-Dynamical-Systems-Edward-Ott/dp/0521010845)，世界图书出版公司出版了该书的影印版，价格更为亲民，推荐购买。由于时间所限，本学期并未学习后两章的内容。

本项目包含的内容主要分为两部分：1.部分课后作业的解答，2.书上一些结果（主要是一些图）的重复。除了理论推导的部分都有产生结果的源代码。

# 使用方法

## 文档部分

本仓库下面包含了两个`.pdf`文档，分别是课后习题解答和结果重复。

两个`.mkdn`文档为`.pdf`文档的源码，至于如何将其编译成`.pdf`文件，可以参考[这里](http://yangkc.gitcafe.io/category/toolbox.html)的“如何写一份优雅的文档”系列。

## 代码部分

本项目的代码都由Python2.7写成，虽然Python的运行效率不太理想，但这里所处理的问题都是动力系统，计算量并不大，所以使用Python也能在较短的计算时间下取得比较理想的结果。

计算和绘图主要依赖Numpy和Matplotlib，并使用了Ipython notebook。

所有的代码都放在`code`目录下。其中`Maps.py`提供了绘制`.pdf`文档中大部分图的基本库，具体使用方法可以参考目录下的文档。

具体的绘图用代码则放在`ploter.ipynb`中，需要使用Ipython notebook打开，建议将Ipython升级到最新版本。

# 注意

由于个人精力所限，完成的作业只有部分，并且由于本书并没有已知的参考答案，所以并不能保证结果是正确的，仅供参考。

# todo

- 工作重复部分找书中的对照
