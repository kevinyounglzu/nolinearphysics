# -*- coding: utf8 -*-
from Maps import *
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    henon = HenonMap(1.4, 0.3, [0, 0])
    series = henon.boundedSeries(20, 1000)
