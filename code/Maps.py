# -*- coding: utf8 -*-
import unittest
import math
import numpy as np


class BaseMap(object):
    """
    The base object for all the other maps.
    """
    def __init__(self, initial_state):
        self.initial_state = initial_state
        self.initialize()

    def initialize(self):
        """
        Change the state of this map to initial state.
        """
        self.state = self.initial_state

    def themap(self, state):
        """
        The map itself.
        @param state: a list of the sates of the map
        """
        return state

    # Make the map instances callable.
    __call__ = themap

    def next(self):
        self.state = self.themap(self.state)

        return self.state

    def __iter__(self):
        """
        The python protocol to make the map instances iterable.
        """
        return self

    def boundedSeries(self, end=50):
        """
        @param end: Integer, the length of the list you need.
        @return: A list contains first end state of the map.
        """
        # store the current state of the map
        current_state = self.getState()
        # initialize the map
        self.initialize()

        series = []
        for i in range(end):
            series.append(self.next())

        # change the state back to where it was before this method had been
        # called.
        self.setState(current_state)
        return series

    def getState(self):
        """
        @return: The current state of this map.
        """
        return self.state

    def setState(self, state):
        """
        Set the state of the map.
        """
        self.state = state


class HenonMap(BaseMap):
    """
    Class for Henon Map:
        x_{n+1} = A - x_{n}^2 + B * y_n
        y_{n+1} = x_n
    """
    def __init__(self, A, B, initial_state):
        """
        @param A: Parameter A
        @param B: Parameter B
        @param initial_state: A list (x, y) of the initial state.
        """
        self.A = A
        self.B = B
        self.initial_state = list(initial_state)

        self.initialize()

    def themap(self, state):
        """
        The map itself.
        @param state: a list of the sate of the map
        """
        x, y = state

        x_next = self.A - x**2 + self.B * y
        y_next = x

        return [x_next, y_next]

    __call__ = themap

    def __repr__(self):
        x, y = self.state
        return "<HenonMap object with A=%f B=%f at (%f, %f)>"\
            % (self.A, self.B, x, y)

    def lyapunovExponent(self, vec):
        current_state = self.getState()
        self.initialize()

        trans = 1000
        steps = 1000
        for i in range(trans):
            self.next()

        v = Vector(vec)
        result = []
        for i in range(1, steps):
            self.next()
            x = self.state[0]
            v.dotByJacobian(x, self.B)
            result.append(math.log(v.normalize()))

        h1 = np.mean(result)
        h2 = math.log(abs(self.B)) - h1
        lyapunovDim = 1 + h1 / abs(h2)

        self.setState(current_state)

        return h1, h2, lyapunovDim


class ReversedHenonMap(BaseMap):
    """
    Class for reversed Henone Map:
        x_{n+1} = y_n
        y_{n+1} = (y^2 + x - A) / B
    """
    def __init__(self, A, B, initial_state):
        """
        @param A: Parameter A
        @param B: Parameter B
        @param initial_state: A list (x, y) of the initial state.
        """
        self.A = A
        self.B = B
        self.initial_state = list(initial_state)

        self.initialize()

    def themap(self, state):
        """
        The map itself.
        @param state: a list of the sate of the map
        """
        x, y = state

        x_next = y
        y_next = (y**2 + x - self.A) / self.B

        return [x_next, y_next]

    __call__ = themap

    def __repr__(self):
        x, y = self.state
        return "<ReversedHenonMap object with A=%f B=%f at (%f, %f)>"\
            % (self.A, self.B, x, y)


class LogisticMap(BaseMap):
    """
    Class for logistic map:
        x_{n+1} = r * x * (1 - x)
    """
    def __init__(self, r, initial_state):
        """
        @param r: Parameter r.
        @param initial_state: A list (x, y) of the initial state.
        """
        self.r = r
        self.initial_state = initial_state
        self.initialize()

    def themap(self, state):
        """
        Logistic map itself.
        @param state: a list of the sate of the map.
        """
        x = state
        return self.r * x * (1 - x)

    def derivative(self, state):
        x = state
        return self.r - 2 * self.r * x

    def lyapunovExponent(self):
        current_state = self.getState()
        self.initialize()

        for i in range(500):
            self.next()

        le = 1
        for i in range(1, 1000):
            le = le * self.derivative(self.state)
            try:
                result = math.log(abs(le)) / i
            except ValueError:
                break
            self.next()

        self.setState(current_state)

        return result

    __call__ = themap

    def __repr__(self):
        return "<Logistic Map with r=%f at x=%f>"% (self.r, self.state)


class BakersMap(BaseMap):
    """
    Class for Baker's Map:
        x_{n+1} = \lambda_a x_n                     if y_n < \alpha
                = (1 - \lambda_b) + \lambda_b x_n   if y_n > \alpha

        y_{n+1} = y_n / \alpha              if y_n < \alpha
                = (y_n - \alpha) / \beta    if y_n > \alpha

        where \beta = 1 - \alpha and \lambda_a + \lambda_b <= 1.
    """
    def __init__(self, lambda_a, lambda_b, alpha, initial_state):
        self.lambda_a = lambda_a
        self.lambda_b = lambda_b
        self.alpha = alpha
        self.beta = 1 - alpha
        self.initial_state = initial_state

        self.initialize()

    def themap(self, state):
        x, y = state

        if y < self.alpha:
            x1 = self.lambda_a * x
            y1 = y / self.alpha
        else:
            x1 = (1 - self.lambda_b) + self.lambda_b * x
            y1 = (y - self.alpha) / self.beta
        return [x1, y1]

    __call__ = themap

    def __repr__(self):
        return """
                <BaseMap object with lambda_a=%f lambda_b=%f alpha=%f
                at (%f, %f)>
               """ % (self.lambda_a, self.lambda_b, self.alpha, self.state[0], self.state[1])


class Vector(object):
    """
    Class for vectors in math.
    """
    def __init__(self, vec):
        """
        @param vec: A list of values to represent a vector.
        """
        self.vec = vec
        self.dim = len(vec)

    def normalize(self):
        n = math.sqrt(sum(map(lambda x: x**2, self.vec)))
        self.vec = map(lambda x: x / float(n), self.vec)
        return n

    def dotByJacobian(self, x, b):
        """
        @param x: The value of x
        @param b: The parameter b

        Say J is the Jacobian matrix of henon map, and v is an instance of Vector.
        This method give the result of J * v.

        Note that J only relies on x and b:
            J = [[-2 * x, b], [1, 0]]
        """
        new_x = - 2 * x * self.vec[0] + b * self.vec[1]
        new_y = self.vec[0]

        self.vec = [new_x, new_y]


class ArnoldCircle(BaseMap):
    """
    Class for arnold circle map:
        \theta_{n+1} = (\theta_n + \omega + k \sin{\theta_n}) \quad \text{modulo} \quad 2 \pi
    """
    def __init__(self, k, omega, initial_state):
        self.k = k
        self.omega = omega
        self.initial_state = initial_state

        self.initialize()

    def themap(self, state):
        theta = state
        return (theta + self.omega + self.k * math.sin(theta)) % np.pi

    __call__ = themap

    def __repr__(self):
        return "<Arnold circle map with k=%f omega=%f at %f>" % (self.k, self.omega, self.state)


class GM(BaseMap):
    """
    Class for map:
        \theta_{n+1} &= \theta_n + a \sin{2 \theta_n} - b \sin{4 \theta_n} - x_n \sin{\theta_n} \\
        x_{n+1} &= - J \cos{\theta_n}
    """
    def __init__(self, J, a, b, initial_state):
        self.J = J
        self.a = a
        self.b = b
        self.initial_state = initial_state

        self.initialize()

    def themap(self, state):
        theta, x = state

        theta1 = theta + self.a * math.sin(2 * theta)\
            - self.b * math.sin(4 * theta) - x * math.sin(theta)
        x1 = - self.J * math.cos(theta)

        return [theta1, x1]

    __call__ = themap


class TestHenonMap(unittest.TestCase):
    def setUp(self):
        self.henon = HenonMap(1.3, 0.4, [0, 0])

    def test_init(self):
        self.assertEqual(1.3, self.henon.A)
        self.assertEqual(0.4, self.henon.B)
        x, y = self.henon.getState()
        self.assertEqual(0, x)
        self.assertEqual(0, y)

    def test_themap(self):
        x1, y1, = self.henon.themap([0, 0])
        self.assertEqual(x1, 1.3)
        self.assertEqual(y1, 0)

    def test_call(self):
        x1, y1, = self.henon([0, 0])
        self.assertEqual(x1, 1.3)
        self.assertEqual(y1, 0)

    def test_next(self):
        x1, y1, = self.henon.next()
        self.assertEqual(x1, 1.3)
        self.assertEqual(y1, 0)

    def test_bounded_series(self):
        series = []
        for i in range(20):
            series.append(self.henon.next())

        x_old, y_old = self.henon.getState()

        bseries = self.henon.boundedSeries(20)

        for a, b in zip(series, bseries):
            self.assertEqual(a[0], b[0])
            self.assertEqual(a[1], b[1])

        x, y = self.henon.getState()

        self.assertEqual(x_old, x)
        self.assertEqual(y_old, y)


class TestLogisticMap(unittest.TestCase):
    def setUp(self):
        self.logistic = LogisticMap(1, 0.5)

    def test_init(self):
        self.assertEqual(self.logistic.r, 1)
        self.assertEqual(self.logistic.getState(), 0.5)

    def test_themap(self):
        self.assertEqual(self.logistic.themap(0.5), 0.25)

    def test_call(self):
        self.assertEqual(self.logistic(0.5), 0.25)

    def test_next(self):
        self.assertEqual(self.logistic.next(), 0.25)

    def test_bounded_series(self):
        series = []
        for i in range(20):
            series.append(self.logistic.next())

        x_old = self.logistic.getState()

        bseries = self.logistic.boundedSeries(20)

        for a, b in zip(series, bseries):
            self.assertEqual(a, b)

        self.assertEqual(x_old, self.logistic.getState())

    def test_lyapunov_exp(self):
        self.logistic.lyapunovExponent()


class TestBakersMap(unittest.TestCase):
    def setUp(self):
        self.baker = BakersMap(0.5, 0.5, 0.4, [0.1, 0.1])

    def test_init(self):
        self.assertEqual(self.baker.lambda_a, 0.5)
        self.assertEqual(self.baker.lambda_b, 0.5)
        self.assertEqual(self.baker.alpha, 0.4)
        self.assertEqual(self.baker.beta, 0.6)

        x, y = self.baker.getState()

        self.assertEqual(x, 0.1)
        self.assertEqual(y, 0.1)

    def test_themap(self):
        x, y = self.baker.themap([0.1, 0.1])
        self.assertEqual(x, 0.05)
        self.assertEqual(y, 0.25)

        x, y = self.baker.themap([0.1, 1])
        self.assertEqual(x, 0.55)
        self.assertEqual(y, 1.0)

    def test_call(self):
        x, y = self.baker([0.1, 0.1])
        self.assertEqual(x, 0.05)
        self.assertEqual(y, 0.25)

        x, y = self.baker([0.1, 1])
        self.assertEqual(x, 0.55)
        self.assertEqual(y, 1.0)

    def test_next(self):
        x, y = self.baker.next()
        self.assertEqual(x, 0.05)
        self.assertEqual(y, 0.25)

        self.baker.setState([0.1, 1])
        x, y = self.baker([0.1, 1])
        self.assertEqual(x, 0.55)
        self.assertEqual(y, 1.0)

    def test_bounded_series(self):
        series = []
        for i in range(20):
            series.append(self.baker.next())

        x_old, y_old = self.baker.getState()

        bseries = self.baker.boundedSeries(20)

        for a, b in zip(series, bseries):
            self.assertEqual(a[0], b[0])
            self.assertEqual(a[1], b[1])

        x, y = self.baker.getState()

        self.assertEqual(x_old, x)
        self.assertEqual(y_old, y)


class TestVector(unittest.TestCase):
    def test_init(self):
        l = [1, 2, 3, 4]
        v1 = Vector(l)

        self.assertEqual(v1.vec, l)
        self.assertEqual(v1.dim, len(l))

    def test_normalize(self):
        # 1 d
        v1 = Vector([2])
        n = v1.normalize()
        self.assertEqual(n, 2)
        self.assertEqual(v1.vec[0], 1)

        # 2d
        v2 = Vector([0.5, 0.5])
        n = v2.normalize()
        self.assertEqual(n, math.sqrt(0.5**2 * 2))
        self.assertEqual(v2.vec[0], 1 / math.sqrt(2))
        self.assertEqual(v2.vec[1], 1 / math.sqrt(2))

        # 3d
        v3 = Vector([1, 1, 1])
        n = v3.normalize()
        self.assertEqual(n, math.sqrt(3))
        for item in v3.vec:
            self.assertEqual(item, 1 / math.sqrt(3))

if __name__ == "__main__":
    unittest.main()
