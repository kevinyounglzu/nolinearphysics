%非线性物理部分结果重复
%杨凯程
%2015-06-28

\newpage

本文档只存放了绘图的结果，绘图用的代码可以在ploter.ipynb中找到。

# 计算Logistic Map的李指数

如图(\ref{logistic})。

![Logistic 映射的李指数随r的变化情况。\label{logistic}](./code/pics/rlogistic1.pdf)

# 计算Logistic Map 的密度

原图可以在教材p35和p54找到。

![Logistic Map 在r=3.8时的密度。\label{logistic2}](./code/pics/rlogistic2.pdf)

![Logistic Map 在r=4.0时的密度。\label{logistic3}](./code/pics/rlogistic3.pdf)

# 计算Baker's Map 的李指数

# 画出如下映射的吸引域

\begin{align*}
    \theta_{n+1} &= \theta_n + a \sin{2 \theta_n} - b \sin{4 \theta_n} - x_n \sin{\theta_n} \\
    x_{n+1} &= - J \cos{\theta_n}
\end{align*}

考虑$J=0.3, a = 1.32, b=0.90$的情况，系统中有两个稳定不动点$(\theta, x)=(0, -J)$和$(\pi, J)$。

下面要画出这两个点的吸引域，具体做法是将空间离散成一系列的格点，以这些格点作为初始值代入上面的映射中，看最终落在哪个稳定点上。为了区分，我们将落在$(0, -J)$上的初始点染成黑色，落在另一个点上的初始值染成白色。结果如图(\ref{basin})和图(\ref{basinzoomin})。

![上述映射的吸引域。\label{basin}](./code/pics/basin1.png)

![图(\ref{basin})中取了一部分放大的结果。\label{basinzoomin}](./code/pics/basin2.png)

# 画出Henon map 的稳定和不稳定流形

画 Henon map 稳定流形的方法和上面画吸引域的方法类似，只不过 Henon map 的吸引子是分形的所以无法直接判断映射过后是否落在某个具体的点上，而是需要判断是否跑出吸引子所在的范围。

而在画不稳定流形的时候则需要首先求出 Henon map 的逆映射，然后用相同的办法来处理。

稳定流形和不稳定流形的交集就是所谓的不变子集。

![\label{manifold}](./code/pics/manifold.png)
